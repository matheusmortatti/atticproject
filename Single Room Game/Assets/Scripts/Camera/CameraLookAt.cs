﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAt : MonoBehaviour
{
    public float velocity = 1;
    public float timeToLook = 10.0f;

    [Space]
    public bool useObjectPosition = true;
    public Transform customPosition;

    [Space]
    public bool activate = false;

    private PlayerMotor playerMotor;
    private float timeCount = 0.0f;

    void Awake()
    {
        playerMotor = FindObjectOfType<PlayerMotor>();
    }

    void Update()
    {
        if(activate)
        {
            Vector3 objectPosition = useObjectPosition ? this.transform.position : customPosition.position;
            Quaternion lookRotation = Quaternion.LookRotation(objectPosition  - playerMotor.camera.transform.position);

            Vector3 rotationAmount = Quaternion.Slerp(playerMotor.camera.transform.rotation, lookRotation, velocity * timeCount).eulerAngles - playerMotor.camera.transform.rotation.eulerAngles;
            Debug.Log(rotationAmount);
            playerMotor.ApplyRotation(rotationAmount);

            timeCount += Time.deltaTime;

            if(timeCount > timeToLook)
            {
                activate = false;
                timeCount = 0;
            }
        }
    }

    public void Activate()
    {
        activate = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToItself : MonoBehaviour {

    public bool useLocalRotation = false;

    [SerializeField]
    private GameObject attachedObject;
    private Rigidbody rb_attachedObject;

    public bool IsAttached()
    {
        return attachedObject != null;
    }

    public GameObject accessAttachedObject()
    {
        return attachedObject;
    }

    public bool Attach(GameObject obj)
    {
        if (attachedObject != null || obj == null) return false;

        attachedObject = obj;

        rb_attachedObject = attachedObject.GetComponent<Rigidbody>();
        if(rb_attachedObject != null)
        {
            rb_attachedObject.useGravity = false;
        }

        return true;
    }

    public void ForceAttach(GameObject obj)
    {
        Detach();

        attachedObject = obj;

        rb_attachedObject = attachedObject.GetComponent<Rigidbody>();
        if (rb_attachedObject != null)
        {
            rb_attachedObject.useGravity = false;
        }
    }

    public void Detach()
    {
        if (attachedObject == null) return;

        if (rb_attachedObject != null)
        {
            rb_attachedObject.useGravity = true;
        }

        //IInteractable interactable = attachedObject.GetComponent<IInteractable>();
        //if (interactable != null)
        //    interactable.OnEndInteraction();

        Debug.Log("Detached " + attachedObject.name);

        attachedObject = null;
    }

    public bool DetachIfEquals(GameObject obj)
    {
        if (attachedObject == null || attachedObject != obj) return false;

        if (rb_attachedObject != null)
        {
            rb_attachedObject.useGravity = true;
        }

        IInteractable interactable = attachedObject.GetComponent<IInteractable>();
        if (interactable != null)
            interactable.OnEndInteraction();

        Debug.Log("Detached " + attachedObject.name);

        attachedObject = null;

        return true;
    }

    void Update()
    {
        if(attachedObject != null)
        {
            if(rb_attachedObject != null)
            {
                //rb_attachedObject.MovePosition(this.transform.position);
                //rb_attachedObject.MoveRotation(this.transform.rotation);
                //rb_attachedObject.Sleep();
            }
            else
            {
                
            }
            attachedObject.transform.position = this.transform.position;

            if(useLocalRotation)
                attachedObject.transform.localRotation = this.transform.rotation;
            else
                attachedObject.transform.rotation = this.transform.rotation;
        }
    }
	
}

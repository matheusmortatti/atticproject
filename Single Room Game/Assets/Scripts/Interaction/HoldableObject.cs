﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class HoldableObject : MonoBehaviour, IPhysicsObject, IInteractable {

    public float throwVelocity = 2;

    private AttachToItself playerHandFront;
    private Rigidbody rb;
    private InteractionStateHandler interactionStateHandler;
    private PlayerController playerController;

    // Use this for initialization
    void Start()
    {
        GameObject playerHandObject = GameObject.FindGameObjectWithTag("PlayerHandFront");
        playerHandFront = playerHandObject.GetComponent<AttachToItself>();

        rb = GetComponent<Rigidbody>();
        interactionStateHandler = FindObjectOfType<InteractionStateHandler>();

        playerController = FindObjectOfType<PlayerController>();
    }

    public void SendInteractionMessage(string message)
    {
        playerController.InteractionMessage(message);
    }

    public void SetPickupButtonEvent()
    {
        playerController.RemoveAllAltListenersInteract();
        playerController.SetAltListenerInteract(new UnityAction(Interact));
    }

    public void Interact()
    {
        if (playerHandFront != null)
        {
            if (playerHandFront.Attach(this.gameObject))
            {
                interactionStateHandler.ChangeState(InteractionStateNames.HOLDING);
            }
        }
    }

    public void Throw(Vector3 direction)
    {
        if (playerHandFront != null)
        {
            if (playerHandFront.DetachIfEquals(this.gameObject))
            {
                interactionStateHandler.ChangeState(InteractionStateNames.HANDSFREE);
            }
        }

        rb.velocity = direction * throwVelocity;
    }

    public void OnEndInteraction()
    {
        Debug.Log("Ended Interaction");
        Throw(playerController.transform.forward);
    }
}

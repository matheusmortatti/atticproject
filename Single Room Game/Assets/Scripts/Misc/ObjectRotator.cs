﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour {

    public float rotateSensitivity = 1.0f;
    public bool RotateFromWorldAxis = false;
    private Vector3 rotation;
    private Rigidbody rb;
    private Quaternion initialRotation;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialRotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z, this.transform.rotation.w);
    }
	
	// Update is called once per frame
	void Update () {
        PerformRotation();
	}

    void PerformRotation()
    {
        if (rb != null)
        {
            rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        }
        else
        {
            this.transform.Rotate(this.transform.parent.transform.forward, rotation.x, Space.World);
            this.transform.Rotate(this.transform.parent.transform.up, rotation.y, Space.World);
            this.transform.Rotate(this.transform.parent.transform.right, rotation.z, Space.World);
            //this.transform.Rotate(rotation, RotateFromWorldAxis ? Space.World : Space.Self);
        }

        rotation = Vector3.zero;
    }

    public void ApplyRotation(Vector3 _rotation)
    {
        rotation = _rotation * rotateSensitivity;
    }

    public void ApplyRotationZ(float zRot)
    {
        rotation.z = zRot * rotateSensitivity;
    }

    public void ApplyRotationY(float yRot)
    {
        rotation.y = yRot * rotateSensitivity;
    }

    public void ApplyRotationX(float xRot)
    {
        rotation.x = xRot * rotateSensitivity;
    }

    public void ResetRotation()
    {
        if (rb != null)
        {
            rb.MoveRotation(rb.rotation*initialRotation);
        }
        else
        {
            this.transform.localRotation = initialRotation;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIController : MonoBehaviour {

    public RectTransform interactTextRect;
    public Text interactText;

    private void OnPostRender()
    {
        //Create a new texture with the width and height of the screen
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        //Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        texture.Apply();

        Vector2 p_PosTop, p_PosBottom;
        Vector3[] p_Corners = new Vector3[4];
        interactTextRect.GetWorldCorners(p_Corners);
        p_PosTop = RectTransformUtility.WorldToScreenPoint(Camera.current, p_Corners[1]);
        p_PosBottom = RectTransformUtility.WorldToScreenPoint(Camera.current, p_Corners[3]);


        Color[] rectColor = texture.GetPixels((int)p_PosTop.x, (int)p_PosTop.y, (int)Mathf.Abs(p_PosTop.x - p_PosBottom.x), (int)Mathf.Abs(p_PosTop.y - p_PosBottom.y));

        Color mean = Maths.Mean(rectColor);
        float grey = (mean.r + mean.g + mean.b) / 3;

        interactText.color = new Color(grey, grey, grey);

        Debug.Log(grey);
    }

}
